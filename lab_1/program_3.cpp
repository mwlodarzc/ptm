#include <iostream>
#include <bitset>
#include <chrono>
#include <thread>
#include "GLOBAL.hh"
// prezentacja programu wypisująca zmiany bitów (bez eclipse)
int main()
{
    // std::bitset<8> DDRD = 0b11111111;
    std::bitset<8> PORTD = 0b10000000;
    bool going_right = true;
    while (1)
    {
        for (int i = 0; i < 8; i++)
        {
            if (going_right)
                PORTD = 128 >> i;
            if (!going_right)
                PORTD = 1 << i;
            PORTD = ~PORTD;
            std::cout
                << going_right << " "
                << PORTD << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            PORTD = ~PORTD;
        }
        going_right = !going_right;
    }
}
//  std::this_thread::sleep_for(std::chrono::milliseconds(500));

// program 3 z eclipse
int main()
{
    DDRD = 0b11111111;
    PORTD = 0b10000000;
    bool going_right = true;
    while (1)
    {
        for (int i = 0; i <= 8; i++)
        {
            if (going_right)
                PORTD = 128 >> i;
            if (!going_right)
                PORTD = 1 << i;
            PORTD = ~PORTD;
            _delay_ms(200);
            PORTD = ~PORTD;
        }
        going_right = !going_right;
    }
}